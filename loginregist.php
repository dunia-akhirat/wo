<div class="container" style="margin-top: 50px">
	<div class="row">
		<div class="col m6 s12">
			<form action="?page=registrasi" method="post">
				<div class="card-panel ">
					<h5>Register</h5>
					<div class="input-field">
	                    <input type="text" name="namalengkap" id="namalengkap" required="">
	                	<label>Nama Lengkap</label>
					</div>
					<div class="input-field">
	                    <input type="text" name="jk" id="jk" required="">
	                	<label>Jenis Kelamin</label>
					</div>
					<div class="input-field">
	                    <input type="text" name="username" id="username" required="">
	                	<label>Username</label>
					</div>
					<div class="input-field">
	                    <input type="password" name="password" id="password" required="">
	                	<label>Password</label>
					</div>
					<div class="input-field">
	                    <input type="email" name="email" id="email" required="">
	                	<label>Email</label>
					</div>
					<div class="input-field">
	                    <input type="text" name="alamat" id="alamat" required="">
	                	<label>Alamat</label>
					</div>
					<div class="input-field">
	                    <input type="text" name="no_hp" id="no_hp" required="">
	                	<label>No. Hp</label>
					</div>
					<button type="submit" name="registrasi" class="btn orange darken-3">Register</button>
				</div>
			</form>
		</div>
		<div class="col m6 s12">
			<form action="?page=ceklogin" method="post">
				<div class="card-panel ">
					<h5>Login</h5>
					<div class="input-field">
	                    <input type="text" name="username" id="username">
	                	<label>Username</label>
					</div>
					<div class="input-field">
	                    <input type="password" name="password" id="password">
	                	<label>Password</label>
					</div>
					<button type="submit" class="btn orange darken-3">Login</button>
				</div>
			</form>
		</div>
	</div>
</div>