<?php
// untuk nama kelas harus sama dengan nama file, untuk linux harus hhuruf besar diawal
class crud{
	public function __construct(){
		// Parameter dari PDO ada 3: host,username, dan pass
		$this->db= new PDO("mysql:host=localhost;dbname=wedding_organizer","root","");

	}
	// Public function showadmin(){
		
	// }
	// CRUD Kategori
	public function showkategori(){
		$query = $this->db->query("SELECT * FROM tb_kategori");
		return $query;
	}
	public function addkategori($nama_kategori){
		$query = $this->db->prepare("INSERT INTO tb_kategori(nama_kategori) VALUES (:nama_kategori)");
		$query->bindparam(':nama_kategori', $nama_kategori);
		// nama_kategori diambil dari values sedangkan nama diambil dari parameter kategori.
		$query->execute();
		return $query;
		// return mengambalikan nilai.
	}
	public function detailkategori($id){
		$query=$this->db->query("SELECT * FROM tb_kategori WHERE id_kategori='$id'");
		return $query;
	}

	public function editkategori($id,$nama_kategori){
		$query = $this->db->prepare("UPDATE tb_kategori SET nama_kategori=:nama_kategori WHERE id_kategori='$id'");
		$query->bindparam(':nama_kategori', $nama_kategori);
		$query->execute();
		return $query;
	}
	public function deletekategori($id){
		$query = $this->db->prepare("DELETE FROM tb_kategori WHERE id_kategori='$id'");
		$query->bindparam(':id_kategori', $id);
		$query->execute();
		return $query;
	}
	// CRUD PRODUK
	public function addproduk($id_kategori, $nama, $harga, $stok, $keterangan, $gambar){
		$query = $this->db->prepare("INSERT INTO tb_produk(id_kategori, nama_barang, harga, stok, keterangan, gambar) VALUES (:id_kategori,:nama_barang,:harga,:stok,:keterangan, :gambar)");
		$query->bindparam(':nama_barang', $nama);
		$query->bindparam(':gambar', $gambar);
		$query->bindparam(':id_kategori', $id_kategori);
		$query->bindparam(':harga', $harga);
		$query->bindparam(':stok', $stok);
		$query->bindparam(':keterangan', $keterangan);
		
		// nama_kategori diambil dari values sedangkan nama diambil dari parameter kategori.
		$query->execute();
		return $query;
		// return mengambalikan nilai.
	}
	

	public function showproduct() {
		$query = $this->db->query("SELECT * FROM tb_produk");
		return $query;
	}

	public function tampilproduk($id)
	{
		$query = $this->db->query("SELECT * FROM tb_produk WHERE id_kategori='$id'");
		return $query;
	}

	public function getproduk($id)
	{
		$query = $this->db->query("SELECT * FROM tb_produk WHERE id_produk='$id'");
		return $query;
	}

	public function editproduk($id ,$kategori,$namabar,$harga,$stok,$keterangan,$gambar){
		$query = $this->db->prepare("UPDATE tb_produk SET id_kategori=:id_kategori, nama_barang=:nama_barang, harga=:harga, stok=:stok, keterangan=:keterangan, gambar=:gambar WHERE id_produk='$id'");
		$query->bindparam(':id_kategori', $kategori);
		$query->bindparam(':nama_barang', $namabar);
		$query->bindparam(':harga', $harga);
		$query->bindparam(':stok', $stok);
		$query->bindparam(':keterangan', $keterangan);
		$query->bindparam(':gambar', $gambar);
		$query->execute();
		return $query;
	}

	public function editproduk2($id ,$kategori,$namabar,$harga,$stok,$keterangan){
		$query = $this->db->prepare("UPDATE tb_produk SET id_kategori=:id_kategori, nama_barang=:nama_barang, harga=:harga, stok=:stok, keterangan=:keterangan WHERE id_produk='$id'");
		$query->bindparam(':id_kategori', $kategori);
		$query->bindparam(':nama_barang', $namabar);
		$query->bindparam(':harga', $harga);
		$query->bindparam(':stok', $stok);
		$query->bindparam(':keterangan', $keterangan);
		$query->execute();
		return $query;
	}

	public function hapusproduk($id){
		$query = $this->db->prepare("DELETE FROM tb_produk WHERE id_produk='$id'");
		$query->bindparam(':id_produk', $id);
		$query->execute();
		return $query;
	}

	public function showuser(){
		$query = $this->db->query("SELECT * FROM tb_user");
		return $query;
	}
	public function getuser($id){
		$query = $this->db->query("SELECT * FROM tb_user WHERE id_user='$id'");
		return $query;
	}
	public function adduser($username,$password,$nama,$alamat,$jenis_kelamin,$email,$no_hp){
		$query=$this->db->prepare("INSERT INTO tb_user(username, password, nama, alamat, jenis_kelamin, email, no_hp) VALUES (:username,:password,:nama,:alamat,:jenis_kelamin,:email,:no_hp) ");
		$query->bindparam(':username', $username);
		$query->bindparam(':password', $password);
		$query->bindparam(':nama',$nama);
		$query->bindparam(':alamat',$alamat);
		$query->bindparam(':jenis_kelamin',$jenis_kelamin);
		$query->bindparam(':email',$email);
		$query->bindparam(':no_hp',$no_hp);
		$query->execute();
		return $query;
	}
	public function edituser($id,$username,$password,$nama,$alamat,$jk,$email,$nohp){
		$query = $this->db->prepare("UPDATE tb_user SET username=:username, password=:password, nama=:nama, alamat=:alamat, jenis_kelamin=:jenis_kelamin, email=:email, no_hp=:no_hp WHERE id_user='$id'");
		$query->bindparam(':username', $username);
		$query->bindparam(':password', $password);
		$query->bindparam(':nama', $nama);
		$query->bindparam(':alamat', $alamat);
		$query->bindparam(':jenis_kelamin', $jk);
		$query->bindparam(':email', $email);
		$query->bindparam(':no_hp', $nohp);
		$query->execute();
		return $query;
	}
	public function deleteuser($id){
		$query = $this->db->prepare("DELETE FROM tb_user WHERE id_user='$id'");
		$query->bindparam(':id_user', $id);
		$query->execute();
		return $query;
	}
	
	// CRUD KERANJANG
	public function keranjang($id){
		$query= $this->db->query("SELECT * FROM tb_keranjang WHERE id_user='$id' ");
		return $query;
	}
	// CRUD PEMESANAN
	public function pemesanan() {
		$query = $this->db->query("SELECT id_pemesanan, id_user, status_pembayaran FROM tb_pemesanan ");
		return $query;
	}


	public function loginUser($username, $password) {
		$query = "SELECT * FROM tb_user WHERE username='$username' AND password='$password'";
		$result = $this->db->query($query);
		if($result->rowCount() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
}
