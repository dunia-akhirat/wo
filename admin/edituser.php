<div class="panel panel-default">
	<div class="panel-heading">
		<h3>Edit User</h3>
	</div>
	<div class="panel-body">
	<?php
	if(isset($_GET['id'])){
		$id = $_GET['id'];
		$res = $crud->getuser($id);
		$data = $res->fetch(PDO::FETCH_OBJ);
	}	
	?>
	<form name="edit" method="post" action="?page=edituserproses">
		<input type="hidden" name="id" class="form-control" value="<?php echo $data->id_user;?>">
		<div class="form-group">
			<label>Username</label>
			<input type="text" name="username" class="form-control" value="<?php echo $data->username;?>">
		</div>
		<div class="form-group">
			<label>Password</label>
			<input type="Password" name="password" class="form-control" value="<?php echo $data->password;?>">
		</div>
		<div class="form-group">
			<label>Nama</label>
			<input type="text" name="nama" class="form-control" value="<?php echo $data->nama;?>">
		</div>
		<div class="form-group">
			<label>Alamat</label>
			<input type="text" name="alamat" class="form-control" value="<?php echo $data->alamat;?>">
		</div>
		<div class="form-group">
			<label>Jenis Kelamin</label>
			<input type="text" name="jk" class="form-control" value="<?php echo $data->jenis_kelamin;?>">
		</div>
		<div class="form-group">
			<label>Email</label>
			<input type="text" name="email" class="form-control" value="<?php echo $data->email;?>">
		</div>
		<div class="form-group">
			<label>No. Hp</label>
			<input type="text" name="nohp" class="form-control" value="<?php echo $data->no_hp;?>">
		</div>
		<input type="submit" name="edituser" class="btn btn-primary" value="Edit"/> <br><br>
	</form>
</div>
</div>
