<?php 

if(isset($_POST['cari'])) {
	$kategori = $_POST['kategori'];
	$res = $crud->tampilproduk($kategori);
}
?>
<a href="?page=tambahproduk" class="btn btn-primary"> Tambah <i class="fa fa-plus-square"></i> </a> <br/> <br/>
	<form action="?page=dropdownproduk" method="POST" class="form-horizontal">
		<div class="form-group">
			<div class="row">
				<div class="col-md-4">
					<select name="kategori" class="form-control">
						<?php
						$res1 = $crud->showkategori();
						while ($data1 = $res1->fetch(PDO::FETCH_OBJ)){
							
							?>
							<option value="<?php echo $data1->id_kategori?>"><?php echo $data1->nama_kategori ?></option>
							<?php
						}
						?>	
					</select>
				</div>
				<div class="col-md-3">
					<input type="submit" name="cari" value="cari" class="btn btn-primary">
				</div>
			</div>
		</div>
	</form>
	<br>
<table class="table table-bordered">
	<tr>
		<th>Nama</th>
		<th>Gambar</th>
		<th>Harga</th>
		<th>Stok</th>
		<th width="25%">Keterangan </th>
		<th  width="18%"> Aksi </th>
	</tr>
<?php
while ($data = $res->fetch(PDO::FETCH_OBJ)) {
?>
	<tr>
		<td> <?php echo $data->nama_barang; ?> </td>
		<td> <img src="<?php echo "../gambar/".$data->gambar ?>" width="150" class="img-thumbnail"> </td>
    	<td> <?php echo $data->harga ?> </td>
    	<td> <?php echo $data->stok ?> </td>
    	<td> <?php echo $data->keterangan ?> </td>
    	<td> <a href="?page=editproduk&id=<?php echo $data->id_produk ?>" class="btn btn-primary"> Edit <i class="fa fa-edit"></i> </a> <a href="?page=hapusroduk" class="btn btn-primary"> Hapus <i class="fa fa-trash"></i> </a> <br/> <br/></td>
	</tr>
	<?php
}?>
</table>

