<?php
if(isset($_GET['page'])) {
	$page = $_GET['page'];
	if($page == "beranda") {
		include 'beranda.php';
	}

	elseif($page == "produk") {
		include 'produk.php';
	}
	elseif($page == "tambahproduk") {
		include 'tambahproduk.php';
	}
	elseif($page == "tambahprodukproses") {
		include 'tambahprodukproses.php';
	}
	elseif($page == "dropdownproduk") {
		include 'dropdownproduck.php';
	}
	elseif($page == "editproduk"){
		include 'editproduk.php';
	}
	elseif($page == "editprodukproses"){
		include 'editprodukproses.php';
	}
	elseif($page == "hapusproduk"){
		include 'hapusproduk.php';
	}
	elseif($page == "kategori"){
		include 'kategori.php';
	}
	elseif($page == "tambahkategori"){
		include 'tambahkategori.php';
	}
	elseif($page == "tambahkategoriproses"){
		include 'tambahkategoriproses.php';
	}
	elseif($page == "editkategori"){
		include 'editkategori.php';
	}
	elseif($page == "editkategoriproses"){
		include 'editkategoriproses.php';
	}
	elseif($page == "hapuskategori"){
		include 'hapuskategori.php';
	}
	elseif($page == "user"){
		include 'user.php';
	}
	elseif($page == "tambahuser"){
		include 'tambahuser.php';
	}
	elseif($page == "tambahuserproses"){
		include 'tambahuserproses.php';
	}
	elseif($page == "edituser"){
		include 'edituser.php';
	}
	elseif($page == "edituserproses"){
		include 'edituserproses.php';
	}
	elseif($page == "hapususer"){
		include 'hapususer.php';
	}
	elseif($page == "pemesanan"){
		include 'pemesanan.php';
	}
	// elseif($page == "logout"){
	// 	include 'logut.php';
	// }
} else {
	include 'beranda.php';
}