<div class="panel panel-default">
	<div class="panel-heading">
		<h3>Edit Kategori</h3>
	</div>
	<div class="panel-body">
	<?php
	if(isset($_GET['id'])){
		$id = $_GET['id'];
		$tampil = $crud->detailkategori($id);
		$data1 = $tampil->fetch(PDO::FETCH_OBJ);
	}	
	?>
	<form name="edit" method="post" action="?page=editkategoriproses" enctype="multipart/form-data">

		<div class="form-group">
			<label>ID Kategori</label>
			<input type="text" name="id" class="form-control" value="<?php echo $data1->id_kategori;?>" readonly>
		</div>
		<div class="form-group">
			<label>Nama Kategori</label>
			<input type="text" name="nama_kategori" class="form-control" value="<?php echo $data1->nama_kategori;?>">
		</div>
		<input type="submit" name="editkategori" class="btn btn-primary" value="Edit"/> <br><br>
	</form>
	</div>
</div>