<div class="panel panel-default">
	<div class="panel-heading">
		<h3>Pemesanan</h3>
	</div>
	<div class="panel-body">
<a href="?page=tambahpemesanan" class="btn btn-primary"> Tambah <i class="fa fa-plus-square"></i> </a> <br/> <br/>
<table class="table table-bordered">
		<tr>
			<th>Id Pemesanan</th>
			<th>Id User</th>
			<th>Status Pembayaran</th>
			<th>Aksi</th>
		</tr>
	<?php
	$res = $crud->pemesanan();
	while ($data = $res->fetch(PDO::FETCH_OBJ)) {
	?>
		<tr>
			<td align="center"> <?php echo $data->id_pemesanan; ?> </td>
        	<td align="center"> <?php echo $data->id_user; ?> </td>
        	<td align="center"> <i> <b> <?php echo $data->status_pembayaran; ?> </b></i></td>
        	<td> <a href="?page=detailpemesanan&id=<?php echo $data->id_pemesanan ?>" class="btn btn-primary"> Detail <i class="fa fa-edit"></i></a> </td>
		</tr>
		<?php
	}
	?>
	</table>
	</div>
</div>