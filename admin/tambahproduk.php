<div class="panel panel-default">
	<div class="panel-heading">
		<h3>Tambah Produk</h3>
	</div>
	<div class="panel-body">
	<form name="tambah" method="post" action="?page=tambahprodukproses" enctype="multipart/form-data">
		<div class="form-group">
			<label>Nama Barang</label>
			<input type="text" name="namabar" class="form-control" required="">
		</div>
		<div class="form-group">
			<label>Kategori</label>
			<select name="kategori" class="form-control">
				<?php 
				$res = $crud->showkategori();
				while($data = $res->fetch(PDO::FETCH_OBJ)) {
				?>
				<option value="<?php echo $data->id_kategori; ?>"><?php echo $data->nama_kategori; ?></option>
				<?php
				}
				?>
			</select>
		</div>
		<div class="form-group">
			<label for="foto">Gambar</label>
			<input type="file" name="foto" required="">
		</div>
		<div class="form-group">
			<label for="harga">Harga</label>
			<input type="text" name="harga" class="form-control" required="">
		</div>
		<div class="form-group">
			<label for="stok">Stok</label>
			<input type="number" name="stok" class="form-control" required="">
		</div>
		<div class="form-group"> 
			<label>Keterangan</label>
			<textarea name="keterangan" id="" cols="30" rows="10" class="form-control" required=""></textarea>
		</div>
		<input type="submit" name="tambahproduk" class="btn btn-primary" value="Tambah"/>
	</form>
	<br/>

</div>
</div>