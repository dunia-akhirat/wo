<div class="panel panel-default">
	<div class="panel-heading">
		<h3>Edit Produk</h3>
	</div>
	<div class="panel-body">
	<?php
	if(isset($_GET['id'])){
		$id = $_GET['id'];
		$res1 = $crud->getproduk($id);
		$data1 = $res1->fetch(PDO::FETCH_OBJ);
	}	
	?>
	<form name="edit" method="post" action="?page=editprodukproses" enctype="multipart/form-data">
		<input type="hidden" name="id" class="form-control" value="<?php echo $data1->id_produk;?>">
		<div class="form-group">
			<label>Nama Barang</label>
			<input type="text" name="namabar" class="form-control" value="<?php echo $data1->nama_barang;?>">
		</div>
		<div class="form-group">
			<label>Kategori</label>
			<select name="kategori" class="form-control">
				<?php 
				$res = $crud->showkategori();
				$res3 = $crud->detailkategori($data1->id_kategori);
				$data4 = $res3->fetch(PDO::FETCH_OBJ);
				?>
				<option value="<?php echo $data1->id_kategori; ?>"><?php echo $data4->nama_kategori; ?></option>
				<?php
				while($data = $res->fetch(PDO::FETCH_OBJ)) {
				?>
				<option value="<?php echo $data->id_kategori; ?>"><?php echo $data->nama_kategori; ?></option>
				<?php
				}
				?>
			</select>
		</div>
		<div class="form-group">
			<label for="foto">Gambar</label><br>
			<img src="../gambar/<?php echo $data1->gambar;?>" width ="300" height="250">
			<input type="file" name="foto" >
		</div>
		<div class="form-group">
			<label for="harga">Harga</label>
			<input type="text" name="harga" class="form-control" value="<?php echo $data1->harga;?>">
		</div>
		<div class="form-group">
			<label for="stok">Stok</label>
			<input type="number" name="stok" class="form-control" value="<?php echo $data1->stok;?>">
		</div>
		<div class="form-group"> 
			<label>Keterangan</label>
			<textarea name="keterangan" cols="30" rows="10" class="form-control"><?php echo $data1->keterangan;?></textarea>
		</div>
		<input type="submit" name="editproduk" class="btn btn-primary" value="Edit"/> <br><br>
	</form>

	</div>
</div>

