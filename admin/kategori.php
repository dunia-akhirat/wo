<div class="panel panel-default">
	<div class="panel-heading">
		<h3>Kategori</h3>
	</div>
	<div class="panel-body">
<a href="?page=tambahkategori" class="btn btn-primary"> Tambah <i class="fa fa-plus-square"></i> </a> <br/> <br/>

<table class="table table-bordered">
	<tr>
		<th>ID Kategori</th>
		<th>Nama Kategori</th>
		<th>Aksi</th>
	</tr>

<?php
	$tampil = $crud->showkategori();
	while($data = $tampil->fetch(PDO::FETCH_OBJ)){
?>		
	<tr>
		<td> <?php echo $data->id_kategori ?></td>
		<td> <?php echo $data->nama_kategori ?> </td>
		<td> <a href="?page=editkategori&id=<?php echo $data->id_kategori?>" class="btn btn-primary"> Edit <i class="fa fa-edit"></i></a> 
			<a href="?page=hapuskategori&id=<?php echo $data->id_kategori?>" class="btn btn-primary"> Hapus <i class="fa fa-trash"></i> </a> <br/> <br/> </td>
	</tr>
<?php
	}
?>
</table>
	</div>
</div>
