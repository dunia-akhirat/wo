<div class="panel panel-default">
	<div class="panel-heading">
		<h3>User</h3>
	</div>
	<div class="panel-body">
<a href="?page=tambahuser" class="btn btn-primary"> Tambah <i class="fa fa-plus-square"></i> </a> <br/> <br/>
<table class="table table-bordered">
	<tr align="center">
		<th>Username</th>
		<th>Password</th>
		<th>Nama</th>
		<th>Alamat</th>
		<th>Jenis Kelamin</th>
		<th>Email</th>
		<th>No. Hp</th>
		<th width="18%">Aksi</th>
	</tr>
	<?php
		$tampil = $crud->showuser();
		while ($data=$tampil->fetch(PDO::FETCH_OBJ)){
	?>
		<tr>
			<td><?php echo $data->username;?> </td>
			<td><?php echo $data->password;?> </td>
			<td><?php echo $data->nama;?> </td>
			<td><?php echo $data->alamat;?> </td>
			<td><?php echo $data->jenis_kelamin;?> </td>
			<td><?php echo $data->email;?> </td>
			<td><?php echo $data->no_hp?> </td>
			<td><a href="?page=edituser&id=<?php echo $data->id_user ?>" class="btn btn-primary"> Edit <i class="fa fa-edit"></i> </a>
			<a href="?page=hapususer&id=<?php echo $data->id_user ?>" class="btn btn-primary"> Hapus <i class="fa fa-trash"></i> </a> <br/> <br/></td>
		</tr>
	<?php
		}
	?>
</table>
	</div>
</div>