<!-- Slider -->
<div class="slider">
   <ul class="slides">
      <li>
         <img src="gambar/T1.jpg"> <!-- random image -->
         <div class="caption right-align">
            <h3>This is our Party!</h3>
            <h5 class="light grey-text text-lighten-3">Pernikahan Adat Banjar.</h5>
         </div>
      </li>
      <li>
         <img src="gambar/P2.jpg"> <!-- random image -->
         <div class="caption left-align">
            <h3>This is our Decoration!</h3>
            <h5 class="light grey-text text-lighten-3">Dekorasi dengan Balutan Tema Putih.</h5>
         </div>
      </li>
      <li>
         <img src="gambar/HA1.jpg"> <!-- random image -->
         <div class="caption center-align">
            <h3>This is our Hantaran!</h3>
            <h5 class="light grey-text text-lighten-3">Hantaran With Red Theme.</h5>
         </div>
      </li>
   </ul>
</div>

<!-- content -->
<section>
   <div class="container">
      <div class="row">
         <div class="col m4 s12">
               <div class="card-panel center">
                  <i class="material-icons medium">accessibility account_balance</i>
                  <h5>Gaun & Dekorasi</h5>
                  <p>Kami menyediakan dekorasi dan gaun-gaun pernikahan yang sesuai dengan selera dan tema pernikahan Anda.</p>
               </div>
            </div>
            <div class="col m4 s12">
               <div class="card-panel center">
                  <i class="material-icons medium">restaurant</i>
                  <h5>Cathering</h5>
                  <p>Hidangan yang kami sediakan sesuai dengan keinginan Anda untuk menyajikan sajin terbaik bagi tamu undangan Anda!.</p>
               </div>
            </div>
            <div class="col m4 s12">
               <div class="card-panel center">
                  <i class="material-icons medium">cake</i>
                  <h5>Cakes</h5>
                  <p>Kue dan Pastry tradisional maupun modern sesuai konsep pernikahan Anda, semua tersedia dan harga terjangkau!.</p>
               </div>
            </div>
      </div>
   </div>
</section>