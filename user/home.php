<?php
   session_start();
   include("../lib/crud.php");
   $crud = new crud();
?>
<!DOCTYPE html>
<html>
<head>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <title>My Wedding</title>
   <!--Import Google Icon Font-->
   <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
   <!-- Materialize CSS -->
   <link rel="stylesheet" href="../assets/materialize/css/materialize.min.css">
   <link rel="stylesheet" href="../assets/materialize/css/style.css">
   <link rel="stylesheet" href="../assets/jquery/jquery-3.3.1.min.js">
   <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
</head>
<body>
   <!-- Navbar  -->
   
   <div class="navbar-fixed">
      <nav class="deep-orange darken-1">
         <div class="container">
            <div class="nav-wrapper">
               <a href="#!" class="brand-logo">My Wedding</a>
               <a href="#" data-target="mobile-nav" class="sidenav-trigger"><i class="material-icons">menu</i></a>
               <ul class="right hide-on-med-and-down">
                  <li><a href="?page=beranda">Home</a></li>
                  <li><a href="?page=produk">Produk</a></li>
                  <li><a href="?page=addcart"><i class="material-icons small">shopping_cart</i></a></li>
                  <li><a href="?page=logout"><i class="material-icons small">power_settings_new</i></a></li>
               </ul>
               
               
            </div>
         </div>
      </nav>
   </div>
   <!-- End-Navbar -->

   <!-- Side-nav -->
   <ul class="sidenav" id="mobile-nav">
      <li><a href="?page=beranda">Home</a></li>
      <li><a href="?page=produk">Produk</a></li>
      <li><a href="?page=addcart"><i class="material-icons small">shopping_cart</i></a></li>
      <li><a href="?page=logout"><i class="material-icons small">power_settings_new</i></a></li>
   </ul>
   <!-- End Side-nav -->
   <?php
   


   if(isset($_GET['page'])) {
      $page = $_GET['page'];
      if ($page == 'beranda') {
         include 'beranda.php';
      } 
      elseif($page == 'produk'){
         include 'produk.php';
      }
      elseif($page == 'logout'){
         include '../logout.php';
      }
      elseif($page == 'addcart'){
         include 'keranjang.php';
      }
      
   } else {
      include 'beranda.php';
   }
   ?>


   <script src="../assets/materialize/js/materialize.min.js"></script>
   <script>
      const sidenav = document.querySelectorAll('.sidenav');
      M.Sidenav.init(sidenav);

      const slider = document.querySelectorAll(".slider");
      M.Slider.init(slider, {
         indicators: false,
         height: 500,
         transition: 600,
         interval: 3000
      });
   </script>
   <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
   <script>
            flatpickr("#tgl", {
                altInput: true,
                altFormat: "F j, Y",
                dateFormat: "Y-m-d",
            }); 
         </script>

</body>
</html>