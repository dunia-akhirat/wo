<div class="container">
	<div class="row">
	<?php 
		$query = $crud->showproduct();
		while($data = $query->fetch(PDO::FETCH_OBJ)){
	?>
	<div class="col m4">
		
		<div class="card-panel center">
			<img src="<?php echo "../gambar/".$data->gambar; ?>" width="150" height="200" >
			<br>
			<h5><?php echo $data->nama_barang; ?></h5>
			<p><?php echo "Rp. ".$data->harga ?></p>
				<form action="?page=addcart&id=<?php echo $data->id_produk?>" method="post">
				<div class="card-panel ">
					<div class="input-field">
	                    <input type="text" name="quantity" value="1" id="quantity"/>
	                	<label>Quantity</label>
					</div>
					<div class="input-field">
	                    <input type="hidden" name="hidden_name" value="<?php echo $data->nama_barang; ?>" />
					</div>
					<div class="input-field">
	                    <input type="hidden" name="hidden_harga" value="<?php echo $data->harga; ?>" />
					</div>
					<button type="submit" name="addcart" class="btn orange darken-3">Beli </button>
				</div>
			</form>
		</div>
	</div>
	<?php } ?>	
	</div>
</div>
