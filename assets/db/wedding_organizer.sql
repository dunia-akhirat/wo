-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 19 Des 2018 pada 07.16
-- Versi Server: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wedding_organizer`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id_admin` int(20) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_admin`
--

INSERT INTO `tb_admin` (`id_admin`, `username`, `password`) VALUES
(1, 'azizanasrina', 'aziza123');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_kategori`
--

CREATE TABLE `tb_kategori` (
  `id_kategori` int(10) NOT NULL,
  `nama_kategori` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_kategori`
--

INSERT INTO `tb_kategori` (`id_kategori`, `nama_kategori`) VALUES
(2, 'gaun'),
(3, 'hantaran'),
(4, 'cathering'),
(5, 'kue'),
(6, 'dekorasi'),
(8, 'paket');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_keranjang`
--

CREATE TABLE `tb_keranjang` (
  `id_keranjang` int(20) NOT NULL,
  `id_produk` int(20) NOT NULL,
  `id_user` int(20) NOT NULL,
  `harga` int(30) NOT NULL,
  `tgl_keranjang` date NOT NULL,
  `quantity` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_pemesanan`
--

CREATE TABLE `tb_pemesanan` (
  `id_pemesanan` int(20) NOT NULL,
  `id_user` int(20) NOT NULL,
  `id_barang` int(20) NOT NULL,
  `tgl_acara` date NOT NULL,
  `tgl_pembayaran` date NOT NULL,
  `status_pembayaran` varchar(30) NOT NULL,
  `harga_total` int(30) NOT NULL,
  `quantity` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_pemesanan`
--

INSERT INTO `tb_pemesanan` (`id_pemesanan`, `id_user`, `id_barang`, `tgl_acara`, `tgl_pembayaran`, `status_pembayaran`, `harga_total`, `quantity`) VALUES
(1, 1, 1, '2018-12-22', '2018-12-19', 'Lunas', 1000000, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_produk`
--

CREATE TABLE `tb_produk` (
  `id_produk` int(30) NOT NULL,
  `id_kategori` int(30) NOT NULL,
  `nama_barang` varchar(30) NOT NULL,
  `harga` int(20) NOT NULL,
  `stok` int(20) NOT NULL,
  `keterangan` text NOT NULL,
  `gambar` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_produk`
--

INSERT INTO `tb_produk` (`id_produk`, `id_kategori`, `nama_barang`, `harga`, `stok`, `keterangan`, `gambar`) VALUES
(1, 2, 'Gaun Merah', 1000000, 10, 'Gaun merah dengan paduan payet mewah memeriahkan hari bahagia Anda.', 'D1.jpg'),
(3, 2, 'Gaun Ungu Muda', 800000, 1, 'Gaun Ungu dengan kombinasi payet emas menonjolkan keanggunan penampilan Anda dihari istimewa.', 'D3.jpg'),
(4, 2, 'Gaun Kuning ', 1000000, 1, 'Warna kuning yang sakral bagi suku Banjar menjadikan acara sakral Anda diliputi kehangatan dan keakraban dalam kekeluargaan.', '1541760664836.jpg'),
(5, 2, 'Gaun Biru', 1000000, 1, 'Kombinasi biru muda dan biru tua pada gaun pengantin ini, seakan menegaskan kepada hadirin bahwa Anda dan pasangan akan selalu bersama hingga maut memisahkan.', 'D2.jpg'),
(13, 2, 'Gaun Hijau-Kuning', 1000000, 1, 'Perpaduan dua warna yang berbeda pada gaun ini, seakan menegaskan perpaduan dua hati di hari bahagia.', '1541760667551.jpg'),
(14, 2, 'Gaun Merah Muda', 800000, 1, 'Gaun merah muda membuat acara bahagia Anda menjadi penuh cinta! ', '1541760668824.jpg'),
(15, 2, 'Gaun Hitam', 1000000, 1, 'Gaun hitam dengan paduan putih seperti memberitahukan kepada para hadirin bahwa Anda dan pasangan akan saling menyempurnakan dalam suka dan duka.', '1541760675646.jpg'),
(16, 2, 'Gaun Putih', 800000, 1, 'Gaun putih menjadi pilihan yang tepat bagi Anda yang ingin mengikat janji suci dengan tampilan elegan dalam suasana akrab dan sakral.', '1541760676737.jpg'),
(17, 4, 'Ketupat Kandangan', 15000, 10, '1x pemesanan berisi 30 porsi. 1 porsi terdiri dari sepotong ikan gabus dan 1 1/2 ketupat.', 'F1.jpg'),
(18, 4, 'Lontong', 8000, 10, '1x pemesanan = 30 Porsi. 1 Porsi terdiri dari lauk (telur/udang/gabus/ayam) masak habang dan 2 buah lontong segitiga.', 'F2.jpg'),
(19, 4, 'Masak Habang', 80000, 10, 'Tersedia telur, ayam, dan daging. 1 porsi setara dengan seekor ayam (banyak potongan dapat disesuaikan) atau 25 (biji) telur atau 10 porsi daging ukuran kecil.', 'F3.jpg'),
(20, 4, 'Bistik', 80000, 10, 'Tersedia telur, ayam, dan daging. 1 porsi setara dengan seekor ayam (banyak potongan dapat disesuaikan) atau 25 (biji) telur atau 10 porsi daging ukuran kecil.', 'F5.jpg'),
(21, 4, 'Kareh', 80000, 10, 'Tersedia telur, ayam, dan daging. 1 porsi setara dengan seekor ayam (banyak potongan dapat disesuaikan) atau 25 (biji) telur atau 10 porsi daging ukuran kecil.', 'F7.jpg'),
(22, 4, 'Sate', 1500, 100, 'harga tertera merupakan harga satuan. Tersedia sate ayam, kambing, dan daging.', 'F18.jpg'),
(24, 6, 'Dekorasi Outdoor', 20000000, 3, 'Dekorasi sudah termasuk Pelaminan, pajangan (sesuai tema), booth foto standar, dan meja depan.', 'P5.jpg'),
(25, 6, 'Dekorasi Indoor', 20000000, 5, 'sudah termasuk dekorasi pelaminan, pajangan (sesuai dengan tema), booth foto standar, dan meja depan.', 'P4.JPG'),
(26, 5, 'Cup Cake Euforia', 225000, 6, 'Standar terdapat 45 Cup cake pada 3 rak yang disesuaikan dengan tema pernikahan Anda.', 'C2.jpg'),
(27, 5, 'Naked Sunny Cake', 225000, 6, 'Kue penuh warna tanpa forsting, ukuran standar 3 tinggkat. bisa pilih warna sama atau ombre.', 'C3.jpg'),
(28, 5, 'Cake in Harmony', 500000, 3, 'Perpaduan antara cup cake dan kue penganti mewah. pembuatan khas disesuaikan dengan tema.', 'C1.jpg'),
(30, 4, 'Soto', 10000, 100, 'Harga tertera merupakan harga per item dengan isian standar. ', 'F20.jpg'),
(32, 8, 'Gold Package', 32000000, 8, 'Paket dapat dipesan sesuai tema pernikahan (indoor/outdoor). Paket sudah termasuk dekorasi, gaun, make up, hantaran, kue pengantin, hantaran serta makanan untuk  1700 porsi (menu dapat disesuaikan). ', 'H1.png'),
(33, 8, 'Silver Package', 27500000, 8, 'Paket dapat dipesan sesuai tema pernikahan (indoor/outdoor). Paket sudah termasuk dekorasi, gaun, make up, hantaran, kue pengantin, hantaran serta makanan untuk  1000 porsi (menu dapat disesuaikan). ', 'H2.png'),
(34, 8, 'Bronze Package', 23650000, 8, 'Paket dapat dipesan sesuai tema pernikahan (indoor/outdoor). Paket sudah termasuk dekorasi, gaun, make up, hantaran, kue pengantin, hantaran serta makanan untuk  500 porsi (menu dapat disesuaikan). ', 'H5.png'),
(35, 3, 'Hantaran A', 450000, 8, 'Ukuran besar. Terdiri 5 box. Terdiri bahan kayu, plastik, keranjang, dan box mika.  ', 'HA2.jpg'),
(36, 3, 'Hantaran B', 385000, 8, 'Ukuran sedang. Terdiri 7 box. Tersedia bahan kayu, plastik, keranjang dan box mika.', 'HA1.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(10) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(30) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `jenis_kelamin` varchar(10) NOT NULL,
  `email` varchar(30) NOT NULL,
  `no_hp` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `username`, `password`, `nama`, `alamat`, `jenis_kelamin`, `email`, `no_hp`) VALUES
(1, 'azizanasrina', '123', 'aziza nasrina', 'Jln. Perdagangan Komplek Perdagangan Permai II No 26', 'Perempuan', 'azizanasrina@gmail.com', '085751056725'),
(2, 'suedatiamalia', '1234', 'suedati amalia', 'Jln. Kayu Tangi 2 Jalur', 'Perempuan', 'suedati@gmail.com', '-'),
(3, 'nureha', '88', 'nurus', 'Jln. Kuin', 'Perempuan', 'sholehah@gmail.com', '-'),
(4, 'marda', '12', 'marda', 'Jln. Kayu Tangi', 'Perempuan', 'marda@gmail.com', '-'),
(5, 'mumu', '12', 'mukarramah', 'Jln. Handil Bakti', 'Perempuan', 'mumu@gmail.com', '-');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `tb_keranjang`
--
ALTER TABLE `tb_keranjang`
  ADD PRIMARY KEY (`id_keranjang`);

--
-- Indexes for table `tb_pemesanan`
--
ALTER TABLE `tb_pemesanan`
  ADD PRIMARY KEY (`id_pemesanan`);

--
-- Indexes for table `tb_produk`
--
ALTER TABLE `tb_produk`
  ADD PRIMARY KEY (`id_produk`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_admin`
--
ALTER TABLE `tb_admin`
  MODIFY `id_admin` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_kategori`
--
ALTER TABLE `tb_kategori`
  MODIFY `id_kategori` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tb_keranjang`
--
ALTER TABLE `tb_keranjang`
  MODIFY `id_keranjang` int(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_pemesanan`
--
ALTER TABLE `tb_pemesanan`
  MODIFY `id_pemesanan` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_produk`
--
ALTER TABLE `tb_produk`
  MODIFY `id_produk` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
